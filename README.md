## VORTEX LATTICE METHOD

2D Vortex Lattice Method. User can input points and get lift slope estimation based on the vortex lattice method using lifting line theory. Developed to be used as backend for SFML draw your own wing application.  
Note: VLM is an invicid solution. 

## Compiling
```
g++ -I headers/ main.cpp 
```
## Usage
Create vector of point structure that represents an ordered (CW/CCW) outline of the wing. Input amount of panels that user wants to discretize wing.
```
#include <vortexLM/all.hpp>
int main(){
        int num_panels = 20;
        point p1 = {0,0};
        point p2 = {.5,.5};
        point p3 = {.7,.5};
        point p4 = {.2,0};
        std::vector<point> points{p1,p2,p3,p4};
        double lift_slope = calc_lift_slope(points,num_panels); //lift slope in degrees
        std::cout << lift_slope << std::endl;
        return 0;
}
```


