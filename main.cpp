#include <vortexLM/all.hpp>
int main(){
	int num_panels = 20;
//	point p1 = {0,0};
//      point p2 = {.5,.5};
//      point p3 = {.7,.5};
//      point p4 = {.2,0};
	point p1 = {407,584};
	point p2 = {403,251};
	point p3 = {680,255};
	point p4 = {703,584};
//	point p5 = {407,584}

	std::vector<point> points{p4,p3,p2,p1};
	double lift_slope = calc_lift_slope(points,num_panels); //lift slope in degrees
        std::cout << lift_slope << std::endl;
	return 0;
}
