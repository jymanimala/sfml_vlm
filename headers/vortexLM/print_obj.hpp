void print_points(std::vector<point> pts){
for (int i=0; i < pts.size(); i++){
        std::cout << "x: " << (pts[i]).x << " y: " << (pts[i]).y << std::endl;
}
}

void print_line_seg(line_seg l1){
        std::cout << "m:" << l1.m << " b:" <<l1.b << " x:" << l1.x << " vert:" << l1.vert << " horiz:" << l1.horiz <<std::endl;
}

void print_shape(shape obj){
	print_points(obj.points);   
	for(int i = 0; i < obj.sides.size(); i++){
		print_line_seg(obj.sides[i]);
	}
}

void print_panel(panel p1){
std::cout << "xm: " << p1.cp.x << " ym: " << p1.cp.y << " x1n: " << p1.bv1.x << " y1n: " << p1.bv1.y << " x2n: " << p1.bv2.x << " y2n: " << p1.bv2.y << std::endl;
}

void datPoints(std::vector<point> points){
	std::ofstream myfile;
	myfile.open("read_points.dat");
	for(int i=0;i<points.size();i++){
		myfile << points[i].x << " " << points[i].y << "\n";
	}
	myfile.close();
	system("gnuplot read_out.p ");
	system("xdg-open 'points.png'");
}

