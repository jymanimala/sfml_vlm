#include <Eigen/Dense>
std::vector<point> norm(std::vector<point> rpts){
	// resize y_max to represents half span of .5b
	// shift points towards the origin
	double x_min = (rpts[0]).x;
	double y_min = (rpts[0]).y;
	double y_max = (rpts[0]).y;
	for (int i=0; i < rpts.size(); i++){
		if((rpts[i]).x<x_min){
			x_min = (rpts[i]).x;
		}
		if((rpts[i]).y<y_min){
			y_min = (rpts[i]).y;
		}
		if((rpts[i]).y>y_max){
			y_max = (rpts[i]).y;
		}
	}
	double scale_factor = .5/(y_max-y_min);
	for (int i=0; i < rpts.size(); i++){
		rpts[i].x = scale_factor*(rpts[i].x - x_min);
		rpts[i].y = scale_factor*(rpts[i].y - y_min);
	}
	return rpts;

}

std::vector<shape> discretize(shape wing, int num_shapes){
	std::vector<shape>shapes;
	double spacing = .5/num_shapes;
	std::vector<point> temp_points;
	shape temp;
	for(int i = 0; i < num_shapes; i++){
		temp_points = addPoints(pointInShape(wing,0+spacing*i),pointInShape(wing,spacing+spacing*i));
		temp = create_shape(temp_points);
		shapes.push_back(temp);
	}	
	return shapes;
}

double downwash_v(bool starboard, point cp, point bv1, point bv2){
	//starboard = 1, portboard = 0
	double xm = cp.x;
	double ym = cp.y;
	double x1n = bv1.x;
	double y1n = bv1.y;
	double x2n = bv2.x;
	double y2n = bv2.y;
	
	if(starboard==0){
		y2n = -bv1.y;
		y1n = -bv2.y;
		x2n = bv1.x;
		x1n = bv2.x;
	}
	double a = (1.0/sqrt(pow(x1n-xm,2.0)+pow(y1n-ym,2.0))*((x1n-x2n)*(x1n-xm)+(y1n-y2n)*(y1n-ym))-1.0/sqrt(pow(x2n-xm,2.0)+pow(y2n-ym,2.0))*((x1n-x2n)*(x2n-xm)+(y1n-y2n)*(y2n-ym)))/((x1n-xm)*(y2n-ym)-(x2n-xm)*(y1n-ym));
	double b = -(1.0/sqrt(pow(x1n-xm,2.0)+pow(y1n-ym,2.0))*(x1n-xm)-1.0)/(y1n-ym);
	double c = -(1.0/sqrt(pow(x2n-xm,2.0)+pow(y2n-ym,2.0))*(x2n-xm)-1.0)/(y2n-ym);
	double w = (a+b-c);
	return w;
}

double panel_downwash(std::vector<panel>panels,int cp, int p){
	double w= downwash_v(1,panels[cp].cp, panels[p].bv1, panels[p].bv2)+downwash_v(0,panels[cp].cp, panels[p].bv1, panels[p].bv2);

	return w;
}
double calc_lift_slope(std::vector<point>raw_points,int num_panels){
	Eigen::MatrixXd coef(num_panels,num_panels);
	Eigen::VectorXd r_coef = Eigen::VectorXd::Constant(num_panels,-1);
	std::vector<point> points = norm(raw_points);
       	shape wing = create_shape(points);
       	std::vector<shape> shapes = discretize(wing,num_panels);
       	std::vector<panel> panels;
      	for(int i = 0; i<shapes.size(); i++){
		if(shapes[i].points.size()>3){
                	panel temp_panel = create_panel(shapes[i]);
                	panels.push_back(temp_panel);
		}
        }
        	double total_area = 0; //area of one wing
        	for(int i = 0; i < num_panels; i++){
                	total_area += panels[i].area;
                	for(int j = 0; j < num_panels; j++) {
                		coef(i,j) = panel_downwash(panels,i,j);
                	}
        	}
        	double lift_slope = ((M_PI*M_PI)/180)*(1/total_area)*(.5/num_panels)*8*(coef.inverse()*r_coef).sum(); // lift_slope per degree
//		datPoints(addPanels(panels));
	return lift_slope;
	}
