struct point
 {
 	double x,y;
 };
struct line_seg
{
	double m,b,x;
	bool vert;
	bool horiz;
	point start;
	point end;
	

};

struct shape
{
	 std::vector<line_seg>sides;
	 std::vector<point>points;
};

struct panel
{
	double area;
	shape outline;
	line_seg fwd_edge;
	line_seg aft_edge;
	line_seg quarter_chord;
	line_seg three_quarter_chord;
	point bv1; // bound vortex1
	point bv2; // bound vortex2
	point cp; // control point
};
