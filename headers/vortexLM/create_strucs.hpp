bool inRange(float refVal1,float refVal2, float val){
	if(refVal1 > refVal2){
        	std::swap(refVal1,refVal2);
	}
	if(val>=refVal1 && val<=refVal2){
		return true;
	}
	else{
		return false;
	}
}

std::vector<point>addPoints(std::vector<point> p1s, std::vector<point> p2s){
	std::vector<point> total = p1s;
	for(int i = 0; i < p2s.size(); i++){
		total.push_back(p2s[i]);
	}
	return total;	
}

line_seg create_line_seg(point p1, point p2){
        line_seg output;
        output.horiz = false;
        output.vert = false;
	if(p2.x <= p1.x){
		std::swap(p1,p2);
        }
	output.start = p1;
	output.end = p2;
        if(p1.x == p2.x){
		if(p2.y <= p1.y){
			std::swap(p1,p2);
		}
		output.m = 0;
		output.b = 0;
		output.x = p1.x;
                output.vert = true;
        }
        else{
                output.m = (p2.y-p1.y)/(p2.x - p1.x);
                output.b = p1.y -output.m*p1.x;
                output.x = 0;
                if(output.m==0){
                        output.vert = 0;
                        output.horiz = true;
                }
        }
	std::vector<point> limit = {output.start,output.end};
	std::vector<point> real_limit = {p1,p2};
        return output;
}

shape create_shape(std::vector<point> pts){
	std::vector<line_seg>vec_wing;
	for (int i=0; i < pts.size()-1; i++){
		vec_wing.push_back(create_line_seg(pts[i],pts[i+1]));
	}
	vec_wing.push_back(create_line_seg(pts[pts.size()-1],pts[0]));
	shape obj_shape = {vec_wing};
	obj_shape.points = pts;
	return obj_shape;
}

point get_x_point(line_seg l1, double y){
	point final_point;
	final_point.y = y;
	
	if(l1.vert == true){
		final_point.x = l1.x;
	}
	else if(l1.horiz == true && l1.b == y){
		//There is no point needing x value for horizonal line, this prevents
		//endpoints of horizontal line will be calculated from the other lines
		//this prevents values on horizontal line duplicated. 
		final_point.x = -10;
	}
	else{
		final_point.x = (y-l1.b)/l1.m; 
	}
	if(inRange(l1.start.x,l1.end.x,final_point.x)==false || inRange(l1.start.y,l1.end.y,final_point.y)==false){
		final_point.x = -10;
	}
	return final_point;
}
std::vector<point> pointInShape(shape panel,double y){
	std::vector<point> points;
	point temp;
	bool withinShape = true;
	bool first_point = false;
	for(int i = 0; i< panel.sides.size(); i++){
		temp=get_x_point(panel.sides[i],y);
		if(temp.x < 100 && temp.x >= 0){
			points.push_back(temp);
		}
	}
	return points;
}

std::vector<point>addPanels(std::vector<panel> allPanels){
std::vector<point> allPoints = allPanels[0].outline.points;
for(int i=1; i<allPanels.size();i++){
	allPoints = addPoints(allPoints,allPanels[i].outline.points);
}
return allPoints;
}

std::vector<point>addShapes(std::vector<shape> allShapes){
std::vector<point> allPoints = allShapes[0].points;
for(int i=1; i<allShapes.size();i++){
        allPoints = addPoints(allPoints,allShapes[i].points);
}
return allPoints;
}

panel create_panel(shape obj){
	//Need to implement method to avoid triangle panels/ AKA last panel on 
	panel wing_panel;
	wing_panel.outline = obj;
	std::vector<line_seg>non_horiz;
	std::vector<line_seg> horiz;
	line_seg top;
	line_seg bottom;
	for(int i = 0; i < obj.sides.size(); i++){
		if(obj.sides[i].horiz == false){
			non_horiz.push_back(obj.sides[i]);
		}
		else{
			horiz.push_back(obj.sides[i]);
		}
	}
	if(get_x_point(non_horiz[0],obj.points[0].y).x < get_x_point(non_horiz[1],obj.points[0].y).x){
		wing_panel.fwd_edge = non_horiz[0];
		wing_panel.aft_edge = non_horiz[1];
		}
	else{
		wing_panel.fwd_edge = non_horiz[1];
		wing_panel.aft_edge = non_horiz[0];
	}

	if(horiz[0].start.y < horiz[1].start.y){
		bottom = horiz[0];
		top = horiz[1];
	}
	else{
		top = horiz[0];
		bottom = horiz[1];
	}
	point qc1 = {((top.end.x-top.start.x)*.25 + top.start.x),top.start.y};
	point qc2 = {((bottom.end.x-bottom.start.x)*.25 + bottom.start.x),bottom.start.y};
	point tqc1 = {((top.end.x-top.start.x)*.75 + top.start.x),top.start.y};
    point tqc2 = {((bottom.end.x-bottom.start.x)*.75 + bottom.start.x),bottom.start.y};
	wing_panel.quarter_chord = create_line_seg(qc1,qc2);
	wing_panel.three_quarter_chord = create_line_seg(tqc1,tqc2);
	if(wing_panel.quarter_chord.start.y > wing_panel.quarter_chord.end.y){
		wing_panel.bv2 = wing_panel.quarter_chord.start;
		wing_panel.bv1 = wing_panel.quarter_chord.end;
	}	
	else{
		wing_panel.bv1 = wing_panel.quarter_chord.start;
                wing_panel.bv2 = wing_panel.quarter_chord.end;
	}
	double yp = (top.start.y - bottom.start.y)*.5;
	wing_panel.cp = get_x_point(wing_panel.three_quarter_chord,(top.start.y - bottom.start.y)*.5+bottom.start.y);
	wing_panel.outline.points.push_back(wing_panel.bv1);
	wing_panel.outline.points.push_back(wing_panel.bv2);
	wing_panel.outline.points.push_back(wing_panel.cp);
// Area Calculation
	double height = top.start.y - bottom.start.y;
	double top_length = top.end.x - top.start.x;
	double bottom_length = bottom.end.x - bottom.start.x;
	wing_panel.area = (top_length + bottom_length)*height*.5;
return wing_panel;
}
